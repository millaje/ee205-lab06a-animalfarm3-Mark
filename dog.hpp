///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 06a - Animal Farm 3
///
/// @file dog.hpp
/// @version 1.0
///
/// Exports data about all dogs
///
/// @author @todo yourName <@todo yourMail@hawaii.edu>
/// @brief  Lab 06a - AnimalFarm3 - EE 205 - Spr 2021
/// @date   @todo dd_mmm_yyyy
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <string>

#include "mammal.hpp"

namespace animalfarm {

class Dog : public Mammal {
public:
	std::string name;
	
	Dog( std::string newName, enum Color newColor, enum Gender newGender );
	
	const std::string speak();

	void printInfo();
};

} // namespace animalfarm
